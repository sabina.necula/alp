package org.example;

public class Fibonacci {
    public static void main(String[] args) {

        try {
            System.out.println(fibo(5));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    public static int fibo(int n) throws Exception {
        if(n<0)
            throw new Exception("nu pot calcula valoarea fibonacci pt numere negative");
        if(n==0||n==1)
            return 1;
        return fibo(n-1)+fibo(n-2);
    }
}
