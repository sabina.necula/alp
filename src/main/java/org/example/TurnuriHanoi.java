package org.example;

public class TurnuriHanoi {
    public static void main(String[] args){
        hanoi(4,'A','C','B');
    }

    static void hanoi(int n, char from, char to, char aux){
        //from
        if(n==1)
            System.out.println("mut discul "+n+" de pe tija "+from+" pe tija "+to);
        else{
            hanoi(n-1,from,aux,to);
            System.out.println("mut discul "+n+" de pe tiha "+from+" pe tija "+to);
            hanoi(n-1,aux,to,from);
        }
    }
}
