package org.example;


public class Main1 {
    public static void main(String[] args) {
        int suma = 140;
        afisezMonede(suma);
    }

    static void afisezMonede(int s) {
        if (s < 3) {
            System.out.println("nu am solutie");
            return;
        }
        boolean solutieGasita = false;
        for (int nrMoneziDe5 = 0; nrMoneziDe5 * 5 <= s; nrMoneziDe5++) {
            int rest = s - (nrMoneziDe5 * 5);
            if (rest % 3 == 0) {
                int nrMonezide3 = rest / 3;
                System.out.println("nr monezi de 3= " + nrMonezide3 + ", nr monezi de 5= " + nrMoneziDe5);
                solutieGasita = true;
            }
        }
        if (!solutieGasita) {
            System.out.println("nu am solutie");
        }
    }
}
